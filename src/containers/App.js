import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import './App.css';

import EditTask from '../components/EditTask/EditTask';
import TaskList from "../components/TaskList/TaskList";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={TaskList} />
          <Route path="/:id/edit" component={EditTask} />
          <Route render={() => <h1>Not found</h1>} />
        </Switch>
      </BrowserRouter>
    )
  }
}



export default App;
