import {
  ADD_TASK,
  DELETE_TASK,
  TODO_ERROR,
  TODO_REQUEST,
  TODO_SUCCESS,
  TODO_REQUEST_SUCCESS,
  EDIT_TASK
} from "./actions";

const initialState = {
    task: [],
    editTask: '',
    loading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK:
            return {
                ...state,
            };
        case EDIT_TASK:
          return {
            ...state,
            editTask: action.title
          };
        case DELETE_TASK:
            return {
                ...state,
            };
        case TODO_REQUEST:
            return {
                ...state,
                loading: true
            };
        case TODO_REQUEST_SUCCESS:
            return {...state, loading: false};
        case TODO_SUCCESS:
            return {
                ...state,
                task: action.title,
                loading: false
            };
        case TODO_ERROR:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export default reducer;
