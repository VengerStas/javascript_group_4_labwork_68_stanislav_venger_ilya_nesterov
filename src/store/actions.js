import axios from '../axiosBase';

export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';
export const EDIT_TASK = 'EDIT_TASK';

export const TODO_REQUEST = 'TODO_REQUEST';
export const TODO_REQUEST_SUCCESS = 'TODO_REQUEST_SUCCESS';
export const TODO_SUCCESS = 'TODO_SUCCESS';
export const TODO_ERROR = 'TODO_ERROR';

export const editTaskSuccess = (title) => {
  return {type: EDIT_TASK, title}
};

export const toDoRequestSuccess = () => {
  return {type: TODO_REQUEST_SUCCESS}
};
export const todoRequest = () => {
  return {type: TODO_REQUEST};
};

export const todoSuccess = title => {
  return {type: TODO_SUCCESS, title};
};

export const todoError = error => {
  return {type: TODO_ERROR, error};
};

export const fetchTodo = () => {
  return (dispatch, getState) => {
    dispatch(todoRequest());
    axios.get('/todo.json').then(response => {
      dispatch(toDoRequestSuccess());
      dispatch(todoSuccess(response.data));
    }, error => {
      dispatch(todoError(error));
    });
  }
};

export const addTask = (task) => {
  return (dispatch) => {
    dispatch(todoRequest());
    axios.post('/todo.json', task).then(() => {
      dispatch(toDoRequestSuccess());
      dispatch(fetchTodo());
    })
  }
};

export const deleteTask = (taskId) => {
  return (dispatch) => {
    dispatch(todoRequest());
    axios.delete('/todo/' + taskId + '.json').then(() => {
      dispatch(fetchTodo());
    })
  }
};

export const editTask = (itemId) => {
  return (dispatch) => {
    dispatch(todoRequest());
    return axios.get('todo/' + itemId + '.json').then((response) => {
      dispatch(editTaskSuccess(response.data.title));
      dispatch(toDoRequestSuccess());
    })
  }
};

export const saveEditTask = (itemId, task) => {
  return (dispatch) => {
    dispatch(todoRequest());
    axios.put('todo/' + itemId + '.json', task).then(() => {
      dispatch(toDoRequestSuccess());
      dispatch(fetchTodo());
    })
  }
};
