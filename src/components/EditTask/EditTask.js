import React, {Component} from 'react';
import {connect} from "react-redux";
import {saveEditTask, editTask} from "../../store/actions";
import './EditTask.css';

class EditTask extends Component {
  state = {
    task: ''
  };

  componentDidMount() {
    this.props.editTask(this.props.match.params.id).then(() => {
      this.setState({
        task: this.props.editDataTask
      })
    });
  }

  editToDo = () => {
    const taskEdit = {title: this.state.task};
    this.props.saveEditTask(this.props.match.params.id, taskEdit);
    this.props.history.push('/');
  };

  editToDoTitle = event => {
    this.setState({[event.target.name] : event.target.value})
  };

  render() {
    return (
      <div className="edit-block">
        <input type="text" name='task' onChange={this.editToDoTitle}  value={this.state.task} className="edit-input"/>
        <button onClick={this.editToDo} className="save">save</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    task: state.task,
    editDataTask: state.editTask
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editTask: (itemId) => dispatch(editTask(itemId)),
    saveEditTask: (itemId, task) => dispatch(saveEditTask(itemId, task)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditTask);
