import React, {Component} from 'react';
import AddTask from "../AddTask/AddTask";
import {deleteTask, fetchTodo} from "../../store/actions";
import Spinner from "../UI/Spinner/Spinner";
import {connect} from "react-redux";
import './TaskList.css';

class TaskList extends Component {
  componentDidMount() {
    this.props.loadData();
  };

  changeTask = id => {
    this.props.history.push(id + '/edit');
  };

  render() {
    let obj = Object.keys(this.props.task).map((id) => (
        <div key={id} className="task-item">
          <p className="task">{this.props.task[id].title}</p>
          <button className="edit" onClick={() => this.changeTask(id)}>Edit</button>
          <button className="delete" onClick={() => this.props.deleteTask(id)}>X</button>
        </div>
    ));
    return (
      <div className="task-block">
        <AddTask/>
        {this.props.loading ? <Spinner/> : obj}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    task: state.task,
    loading: state.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadData: () => dispatch(fetchTodo()),
    deleteTask: (taskId) => dispatch(deleteTask(taskId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskList);
