import React, {Component} from 'react';
import {connect} from "react-redux";
import {addTask} from "../../store/actions";

import "./AddTask.css";

class AddTask extends Component {
state = {
    todoTask: ''
};
  postToDo = () => {
      const task = {title: this.state.todoTask};
      this.props.addTask(task);
  };

  changeToDo = (event) => {
      this.setState({[event.target.name] : event.target.value})
  };

  render() {
    return (
      <div>
        <input type="text" className="add-task" name='todoTask' onChange={this.changeToDo} value={this.state.todoTask}/>
        <button onClick={this.postToDo} className="add">Add</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      taskToDo: state.taskToDo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTask: (task) => dispatch(addTask(task)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);
